<?php 
	class Product {
		protected $name;
		protected $price;
		protected $description;
		protected $category;
		protected $stockNo;

		public function __construct($name, $price, $description, $category, $stockNo){
			$this->name = $name;
			$this->price = $price;
			$this->description = $description;
			$this->category = $category;
			$this->stockNo = $stockNo;
		}
		public function printDetails(){
			return "The product has a name of $this->name and its price is $this->price, and the stock no is $this->stockNo";
		}

		public function getPrice(){
			return $this->price;
		}

		public function getStockNo(){
			return $this->stockNo;
		}

		public function getCategory(){
			return $this->category;
		}
		
		public function setPrice($price){
			$this->price = $price;
		}

		public function setStockNo($stockNo){
			$this->stockNo = $stockNo;
		}
		
		public function setCategory($category){
			$this->category = $category;
		}
		
	}

	class Mobile extends Product {
		public function printDetails(){
			return "Our latest mobile is $this->name with a cheapest price of $this->price";
		}
	}

	class Computer extends Product {
		public function printDetails(){
			return "Our newest computer is $this->name and its base price is $this->price";
		}
	}

	$newProduct = new Product('Xioami Mi Monitor', 22000, 'Good for gaming and for coding', 'computer-peripherals', 5);

	$newMobileProduct = new Mobile('Xioami Redmi Note 10 pro', 13590, 'Latest Xioami phone ever made', 'mobiles and electronics', 10);

	$newComputerProduct = new Computer('HP Business Laptop', 29000, 'HP Laptop only made for business', 'laptops and computer', 10);
?>
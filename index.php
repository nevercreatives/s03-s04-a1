<?php require './code.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP OOP | Activity</title>
</head>
<body>
	<h1>PHP OOP | Activity</h1>
	
	<ul>
		<li>
			<?php
				echo $newProduct->printDetails();
			?>		
		</li>
		<li>
			<?php
				echo $newMobileProduct->printDetails();
			?>	
		</li>
		<li>
			<?php
				echo $newComputerProduct->printDetails();
			?>	
		</li>
	</ul>

	<ul>
		<li>
			<?php 
				$newProduct->setStockNo(3);
				echo $newProduct->getStockNo();
			?>
		</li>
		<li>
			<?php
				$newMobileProduct->setStockNo(5);
				echo $newMobileProduct->getStockNo();
			?>
		</li>
		<li>
			<?php 
				$newComputerProduct->setCategory('laptops, computers and electronics');
				echo $newComputerProduct->getCategory();
			?>
		</li>
	</ul>
</body>
</html>